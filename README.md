CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The remote image module allows us to use images via URLs, but still, renders
out the image URL as an image. It provides a new field type 'Remote Image'
which you can add in any of your entities to provide users a feature of just
supplying the image link instead of uploading an image. The users also get
control of the alternate text, title, and height/width of the image. It is
useful in scenarios where image data of your website is stored on a different
server and is accessible via a URL. It currently doesn't support applying image
styles to images placed using a remote image field. Along with a default
formatter, it also provides a formatter that displays meta information
of the image as well.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/remote_image

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/remote_image


Requirements
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Go to manage field section of any entity and click on add field.

 * Add a new type of field 'Remote Image' under the reference sub-type.

 * Configure the default settings, widget settings and required formatter.

 * Start adding images simply using an external URL.

MAINTAINERS
-----------

Current maintainers:
 * Daniel Wehner (dawehner) - https://www.drupal.org/u/dawehner
 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor
