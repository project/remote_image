<?php

namespace Drupal\remote_image\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'remote_image' widget.
 *
 * @FieldWidget(
 *   id = "remote_image",
 *   label = @Translation("Remote Image"),
 *   field_types = {
 *     "remote_image"
 *   }
 * )
 */
class RemoteImageWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder_url' => '',
      'placeholder_title' => '',
      'placeholder_alt' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Get the field settings.
    $field_settings = $this->fieldDefinition->getSettings();
    $elements = parent::settingsForm($form, $form_state) + [
      'placeholder_alt' => ($field_settings['alt_attribute'] === DRUPAL_DISABLED) ? [] : [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder for alt text'),
        '#default_value' => $this->getSetting('placeholder_alt'),
        '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
      ],
    ];

    $elements['placeholder_title'] = ($field_settings['title_attribute'] === DRUPAL_DISABLED) ? [] : [
      '#title' => $this->t('Placeholder for image title'),
      '#states' => [],
    ] + $elements['placeholder_title'];

    return $elements;
  }

  /**
   * Form element validation handler for the 'alt' element.
   *
   * Conditionally requires the image alt if a URL value was filled in.
   */
  public static function validateAltElement(&$element, FormStateInterface $form_state, $form) {
    if ($element['uri']['#value'] !== '' && $element['alt']['#value'] === '') {
      $form_state->setError($element['title'], t('@alt field is required if there is @uri input.', ['@alt' => $element['alt']['#title'], '@uri' => $element['uri']['#title']]));
    }
  }

  /**
   * Form element validation handler for the 'alt' element.
   *
   * Requires the URL value if an image alt was filled in.
   */
  public static function validateAltNoLink(&$element, FormStateInterface $form_state, $form) {
    if ($element['uri']['#value'] === '' && $element['alt']['#value'] !== '') {
      $form_state->setError($element['uri'], t('The @uri field is required when the @alt field is specified.', ['@alt' => $element['alt']['#title'], '@uri' => $element['uri']['#title']]));
    }
  }

  /**
   * Form element validation handler for the 'width' element.
   *
   * Requires the URL value if an image width was filled in.
   */
  public static function validateWidthNoLink(&$element, FormStateInterface $form_state, $form) {
    if ($element['uri']['#value'] === '' && !empty($element['width']['#value'])) {
      $form_state->setError($element['uri'], t('The @uri field is required when the @width field is specified.', ['@width' => $element['width']['#title'], '@uri' => $element['uri']['#title']]));
    }
  }

  /**
   * Form element validation handler for the 'height' element.
   *
   * Requires the URL value if an image height was filled in.
   */
  public static function validateHeightNoLink(&$element, FormStateInterface $form_state, $form) {
    if ($element['uri']['#value'] === '' && !empty($element['height']['#value'])) {
      $form_state->setError($element['uri'], t('The @uri field is required when the @height field is specified.', ['@height' => $element['height']['#title'], '@uri' => $element['uri']['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#type'] = 'fieldset';
    $element['uri']['#title'] = $this->t('URL');
    $element['uri']['#weight'] = 5;

    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $items[$delta];
    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternative text'),
      '#description' => $this->t('This text will be used by screen readers, search engines, and when the image cannot be loaded.'),
      '#default_value' => $item->alt,
      '#placeholder' => $this->getSetting('placeholder_alt'),
      '#access' => $this->getFieldSetting('alt_attribute') != DRUPAL_DISABLED,
      '#required' => $this->getFieldSetting('alt_attribute') === DRUPAL_REQUIRED && $element['#required'],
      '#weight' => 10,
      '#maxlength' => 512,
    ];
    if (!$this->isDefaultValueWidget($form_state) && $this->getFieldSetting('alt_attribute') === DRUPAL_REQUIRED) {
      $element['#element_validate'][] = [get_called_class(), 'validateAltElement'];
      $element['#element_validate'][] = [get_called_class(), 'validateAltNoLink'];

      if (!$element['alt']['#required']) {
        // Make alt required on the front-end when URI filled-in.
        $field_name = $this->fieldDefinition->getName();

        $parents = $element['#field_parents'];
        $parents[] = $field_name;
        $selector = $root = array_shift($parents);
        if ($parents) {
          $selector = $root . '[' . implode('][', $parents) . ']';
        }

        $element['alt']['#states']['required'] = [
          ':input[name="' . $selector . '[' . $delta . '][uri]"]' => [
            'filled' => TRUE,
          ],
        ];
      }
    }
    if (!$this->isDefaultValueWidget($form_state) && $this->getFieldSetting('alt_attribute') == DRUPAL_OPTIONAL) {
      $element['#element_validate'][] = [get_called_class(), 'validateAltNoLink'];
    }

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The title attribute is used as a tooltip when the mouse hovers over the image.'),
      '#default_value' => $item->title,
      '#placeholder' => $this->getSetting('placeholder_title'),
      '#access' => $this->getFieldSetting('title_attribute') != DRUPAL_DISABLED,
      '#required' => $this->getFieldSetting('title_attribute') === DRUPAL_REQUIRED && $element['#required'],
      '#weight' => 15,
      '#maxlength' => 1024,
    ];
    if (!$this->isDefaultValueWidget($form_state) && $this->getFieldSetting('title_attribute') === DRUPAL_REQUIRED) {
      $element['#element_validate'][] = [get_called_class(), 'validateTitleElement'];
      $element['#element_validate'][] = [get_called_class(), 'validateTitleNoLink'];

      if (!$element['title']['#required']) {
        // Make title required on the front-end when URI filled-in.
        $field_name = $this->fieldDefinition->getName();

        $parents = $element['#field_parents'];
        $parents[] = $field_name;
        $selector = $root = array_shift($parents);
        if ($parents) {
          $selector = $root . '[' . implode('][', $parents) . ']';
        }

        $element['title']['#states']['required'] = [
          ':input[name="' . $selector . '[' . $delta . '][uri]"]' => [
            'filled' => TRUE,
          ],
        ];
      }
    }
    if (!$this->isDefaultValueWidget($form_state) && $this->getFieldSetting('title_attribute') == DRUPAL_OPTIONAL) {
      $element['#element_validate'][] = [get_called_class(), 'validateTitleNoLink'];
    }

    $element['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#description' => $this->t('The width of the image'),
      '#weight' => 20,
      '#default_value' => $item->width ?: 0,
    ];
    $element['#element_validate'][] = [get_called_class(), 'validateWidthNoLink'];

    $element['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#description' => $this->t('The height of the image.'),
      '#weight' => 25,
      '#default_value' => $item->height ?: 0,
    ];
    $element['#element_validate'][] = [get_called_class(), 'validateHeightNoLink'];

    return $element;
  }

}
