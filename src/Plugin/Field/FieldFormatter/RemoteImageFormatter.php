<?php

namespace Drupal\remote_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'remote_image' formatter.
 *
 * @FieldFormatter(
 *   id = "remote_image",
 *   label = @Translation("Remote Image"),
 *   field_types = {
 *     "remote_image"
 *   }
 * )
 */
class RemoteImageFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Add one image per item.
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'image',
        '#uri' => $item->uri,
        '#attributes' => ['class' => ['remote-image-item']],
      ];

      // Set the width field.
      if (!empty($item->width)) {
        $elements[$delta]['#width'] = $item->width;
      }

      // Set the height field.
      if (!empty($item->height)) {
        $elements[$delta]['#height'] = $item->height;
      }

      // Set the title field.
      if ($this->fieldDefinition->getSetting('title_attribute') != DRUPAL_DISABLED) {
        $elements[$delta]['#title'] = $item->title;
      }

      // Set the alt field.
      if ($this->fieldDefinition->getSetting('alt_attribute') != DRUPAL_DISABLED) {
        $elements[$delta]['#alt'] = $item->alt;
      }
    }

    return $elements;
  }

}
